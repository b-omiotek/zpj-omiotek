package com.omiotek.java.regex;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Z8 {
	public static void main(String[] args) {
		String sciezka="/home/student/m�jplik.txt";
		System.out.println("Podana sciezka: /home/student/m�jplik.txt\n");
		System.out.println("Nazwy katalog�w: "+Arrays.toString(getKatalogi(sciezka)));
		System.out.println("Nazwa pliku: "+getFileName(sciezka));
		System.out.println("Rozszerzenie pliku: "+getFileExtension(sciezka));
	}
	static String[] getKatalogi(String sciezka){
		String temp="";
		Pattern pattern=Pattern.compile("[A-Za-z�����깜��ƥ�ʣ��]+[/\\\\]+");
		Matcher matcher=pattern.matcher(sciezka);
		while(matcher.find())
			temp+=matcher.group().replaceAll("[/\\\\]+","")+",";
		return temp.split(",");
	}
	static String getFileName(String sciezka){
		Pattern pattern=Pattern.compile("[^/\\\\]+$");
		Matcher matcher=pattern.matcher(sciezka);
		if(matcher.find())
			return matcher.group().replaceAll("\\.[a-z]+","");
		else
			return "Nie uda�o si� odnale�� rozszerzenia pliku. Sprawd� �cie�k�.";

	}
	static String getFileExtension(String sciezka){
		Pattern pattern=Pattern.compile("\\.[a-z]+");
		Matcher matcher=pattern.matcher(sciezka);
		if(matcher.find())
			return matcher.group();
		else
			return "Nie uda�o si� odnale�� rozszerzenia pliku. Sprawd� �cie�k�.";
	}
}
