package com.omiotek.java.regex;

import java.util.Scanner;

public class Z5 {
	public static void main(String args[]){
		Scanner s=new Scanner(System.in);
		System.out.println("Podaj wejscie: ");
		String[] slowaWejscie=s.nextLine().split("\\PL+");
		String wynik="";
		for(String c: slowaWejscie){
			if(c.matches("\\bc[a-z]{2}\\b"))
				wynik+=c.toUpperCase()+" ";
			else
				wynik+=c+" ";
		}
		System.out.println(wynik);
	}
}
