package com.omiotek.java.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Z2 {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		while (true) {
			System.out.println("Wprowadz numer domu: ");
			String in = s.nextLine();
			Pattern pattern=Pattern.compile("\\d+[A-Za-z]?([\\\\/]\\d+[A-Za-z]?)?");
			Matcher matcher = pattern.matcher(in);
			if (matcher.matches())
				System.out.println("Poprawny");
			else
				System.out.println("Niepoprawny");
		}
	}
}
