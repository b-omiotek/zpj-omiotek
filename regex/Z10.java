package com.omiotek.java.regex;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Z10 {
	public static void main(String[] args) {
		LocalDate data=LocalDate.now();
		System.out.println(data.format((DateTimeFormatter.ofPattern("dd-MMM-yyyy"))));
		Pattern pattern=Pattern.compile("(\\d{1,2})-([a-z]{3})-(\\d+)");
		Matcher matcher=pattern.matcher(data.format((DateTimeFormatter.ofPattern("dd-MMM-yyyy"))));
		while(matcher.find()){
			System.out.println(matcher.group().replaceAll(pattern.toString(),"Dzie�: $1\nSkr�t miesi�ca: $2\nRok: $3"));
		}
	}
}
