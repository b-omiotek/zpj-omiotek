package com.omiotek.java.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Z1 {
	public static void main(String args[]){
		Scanner s = new Scanner(System.in);
		while (true) {
			System.out.println("Podaj liczbe zmiennoprzecinkowa: ");
			String in = s.nextLine();
			Pattern pattern=Pattern.compile("-?\\d+(.\\d+)?");
			Matcher matcher = pattern.matcher(in);
			if (matcher.matches())
				System.out.println("Poprawna");
			else
				System.out.println("Niepoprawna");
		}
	}
}
