package com.omiotek.java.lab04;

import java.io.File;
import java.io.FileFilter;

public class Z10 {
	public static void main(String[] args) {
        File file = makeFile(".");
        if (test(file) != 0)
            System.exit(0);
        FileFilter filter = fileFilterAnonymousClass(file);
        print(file, filter);
	}
	public static int test(File f){
        if (!f.exists()) {
            System.out.println("Podany plik nie istnieje.");
            return 1;
        } else if (!f.isDirectory()) {
            System.out.println("Podany plik nie jest katalogiem.");
            return 2;
        }
        return 0;
    }

    public static File makeFile(String nameDirectory) {
        try {
            return new File(nameDirectory);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static FileFilter fileFilterAnonymousClass(File pathname) {
        return new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        };
    }
    
    public static FileFilter fileFilterLambda(File pathname) {
        return (File pathname1) -> pathname1.isDirectory();
    }

    public static void print(File f, FileFilter ff) {
        File[] fl = f.listFiles(ff);
        for (File file : fl) {
            System.out.println(file.getName());
        }
    }
}
