package com.omiotek.java.lab04;

public class Z13 {
	public static void main(String[] args) {
		Object[] o = {new InnerClass1(), new InnerClass2(), new InnerClass3(), new String()};
        Runnable rr = runAll(o);
        rr.run();
	}
	public static Runnable runAll(Object[] o) {
        return () -> {
            for (Object i : o) {
                if (i instanceof Runnable) {
                    ((Runnable) i).run();
                }
            }
        };
    }
}

class InnerClass1 implements Runnable {
    @Override
    public void run() {
    	System.out.println(getClass().getSimpleName());
    }
}
class InnerClass2 implements Runnable {
    @Override
    public void run() {
    	System.out.println(getClass().getSimpleName());
    }
}
class InnerClass3 implements Runnable {
    @Override
    public void run() {
    	System.out.println(getClass().getSimpleName());
    }
}
