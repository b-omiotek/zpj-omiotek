package com.omiotek.java.lab04;

import java.io.File;
import java.util.Arrays;

public class Z12 {
	public static void main(String[] args) {
        File directory = new File(".");
        File[] files = directory.listFiles();
        Arrays.sort(files, (f1,f2) -> {
            if (f1.isDirectory() && f2.isDirectory()) {
                return f1.compareTo(f2);
            } else if (f1.isDirectory()) {
                return -1;
            } else if (f2.isDirectory()) {
                return 1;
            }
            return f1.compareTo(f2);
            });
        Arrays.asList(files).forEach((e)->System.out.println(e));
	}
}
