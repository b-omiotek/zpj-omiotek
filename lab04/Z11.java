package com.omiotek.java.lab04;

import java.io.File;
import java.util.Arrays;

public class Z11 {
	public static void main(String[] args) {
        File f = new File(".");
        String[] tf = f.list((dir, name) -> name.endsWith("settings"));
        System.out.println(Arrays.toString(tf));
	}
}
