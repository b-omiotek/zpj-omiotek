package com.omiotek.java.lab04;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Z14 {
	public static void main(String[] args) {
		Employee[] pracownicy = new Employee[5];
        pracownicy[0] = new Employee("Kowalski", 3200.00);
        pracownicy[1] = new Employee("Nowak", 2000.00);
        pracownicy[2] = new Employee("Baczy�ski", 1200.00);
        pracownicy[3] = new Employee("Jankowski", 4200.00);
        pracownicy[4] = new Employee("Gierczy�ski", 2000.00);
        sortuj(pracownicy);
        for(Employee e: pracownicy)
            System.out.println(e);
	}
	public static void sortuj(Employee[] employees) {
        Collections.sort(Arrays.asList(employees),
        		Comparator.comparing(
                        (Employee e) -> e.getSalary(), (s1, s2) -> {
                            return s1.compareTo(s2);
                        }).thenComparing(
                                Employee::getName, (w1, w2) -> {
                                    return w1.compareTo(w2);
                                }));
    }
}

class Employee {
    private String nazwisko;
    private double wynagrodzenie;
        
    public Employee(String nazwisko, double wynagrodzenie) {
        this.nazwisko = nazwisko;
        this.wynagrodzenie = wynagrodzenie;
    }
    public void raiseSalary(double byPercent) {
        double raise = wynagrodzenie * byPercent / 100;
        wynagrodzenie += raise;    
    }
    public String getName() {
        return nazwisko;
    }
    public double getSalary() {
        return wynagrodzenie;
    }
    @Override
    public String toString(){
        return getName() +", " + getSalary()+" z�";
    }
}