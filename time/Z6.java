package com.omiotek.java.time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class Z6 {
	public static void main(String[] args) {
		LocalDate start=LocalDate.of(1900,1,1);
        LocalDate end=LocalDate.of(1999,12,31);
        do{
        	start=start.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
            System.out.println(start);
        }
        while(start.isBefore(end));
	}
}
