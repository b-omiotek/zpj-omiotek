package com.omiotek.java.time;

import java.time.LocalDateTime;

public class TimeInterval {
	private LocalDateTime taskStart;
	private LocalDateTime taskEnd;
	private String taskDesc;
	
	public TimeInterval(LocalDateTime taskStart,LocalDateTime taskEnd, String taskDesc){
		this.taskStart=taskStart;
		this.taskEnd=taskEnd;
		this.taskDesc=taskDesc;
	}
	public boolean isInConflictWith(TimeInterval ti){
		if(this.getTaskStart().isBefore(ti.getTaskStart()))
			if(this.getTaskStart().isBefore(ti.getTaskEnd()))
				if(this.getTaskEnd().isBefore(ti.getTaskStart()))
					if(this.getTaskEnd().isBefore(ti.getTaskEnd()))
						return false;
		return true;
	}
	public LocalDateTime getTaskStart() {
		return taskStart;
	}
	public LocalDateTime getTaskEnd() {
		return taskEnd;
	}
	public String getTaskName() {
		return taskDesc;
	}
}
