package com.omiotek.java.time;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Z11 {
	public static void main(String[] args) {
		ZonedDateTime travelStartTime=ZonedDateTime.of(LocalDate.now(),LocalTime.of(14,5),ZoneId.of("Europe/Berlin"));
		ZonedDateTime travelEndTime=ZonedDateTime.of(LocalDate.now(),LocalTime.of(16,40), ZoneId.of("America/Los_Angeles"));
		Duration travelTime=getTravelTime(travelStartTime,travelEndTime);
		Z10.printTravel(travelStartTime,travelTime,travelEndTime);
	}
	
	static Duration getTravelTime(ZonedDateTime travelStartTime,ZonedDateTime travelEndTime){
		return Duration.between(travelStartTime,travelEndTime);
	}
}
