package com.omiotek.java.time;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Z8 {
	public static void main(String[] args) {
		ZoneId.getAvailableZoneIds()
        .stream()
        .map(ZoneId::of)
        .map(ZonedDateTime::now)
        .map(t->t.plusHours(1))
        .forEach(System.out::println);
	}
}
