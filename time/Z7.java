package com.omiotek.java.time;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Z7 {
	public static void main(String[] args) {
		DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		
		LocalDateTime task1Start=LocalDateTime.parse("10-03-2018 18:00",formatter);
		LocalDateTime task1End=LocalDateTime.parse("10-03-2018 19:30",formatter);
		
		LocalDateTime task2Start=LocalDateTime.parse("10-03-2018 15:00",formatter);
		LocalDateTime task2End=LocalDateTime.parse("10-03-2018 20:00",formatter);
		
		TimeInterval task1=new TimeInterval(task1Start,task1End,"Siłownia");
		TimeInterval task2=new TimeInterval(task2Start,task2End,"Praca");
		
		if(task1.isInConflictWith(task2))
			System.out.println("Ostrzeżenie: wydarzenie \""+task1.getTaskName()+"\" koliduje z wydarzeniem \""+task2.getTaskName()+"\".");
		else
			System.out.println("Pomyślnie utworzono.");
	}
}
