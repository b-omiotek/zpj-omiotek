package com.omiotek.java.time;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Scanner;

public class Z4{
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Podaj rok: ");
		int rok=s.nextInt();
		System.out.println("Podaj numer miesiaca: ");
		rysujKalendarz(rok,s.nextInt());
		s.close();
	}
	
	static void rysujKalendarz(int rok,int miesiac){
		try{
			LocalDate.of(rok,miesiac,1);
		}
		catch(DateTimeException e){
			System.out.println("Podano z�e argumenty.");
			System.exit(1);
		}
		LocalDate data=LocalDate.of(rok,miesiac,1);
		System.out.println("pon\twt\t�r\tczw\tpt\tsob\tnd ");
		
		for(int i=0;i<data.getDayOfWeek().getValue()-1;i++){
			System.out.print("\t");
		}
		for(int dni=1,k=data.getDayOfWeek().getValue();dni<data.lengthOfMonth()+1;dni++,k++){
			System.out.print(dni+"\t");
			if(k%7==0 && k!=0)
				System.out.print("\n");
		}
	}
}
