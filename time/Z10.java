package com.omiotek.java.time;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Z10 {
	public static void main(String[] args) {
		ZonedDateTime travelStartTime=ZonedDateTime.of(LocalDate.now(),LocalTime.of(15,5),ZoneId.systemDefault());
		Duration travelTime=Duration.ofHours(10).plusMinutes(50);
		ZonedDateTime travelEndTime=getTravelEndTime(travelStartTime,travelTime,ZoneId.of("America/Los_Angeles"));
		printTravel(travelStartTime,travelTime,travelEndTime);
	}
	
	static ZonedDateTime getTravelEndTime(ZonedDateTime startTime,Duration travelTime,ZoneId travelGoalZoneId){
		return startTime.plus(travelTime).withZoneSameInstant(travelGoalZoneId);
	}
	
	static void printTravel(ZonedDateTime travelStartTime,Duration travelTime,ZonedDateTime travelEndTime){
		DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		System.out.println("Pocz�tek podr�y: "+travelStartTime.format(formatter));
		
		System.out.println("Czas trwania podr�y: "+String.format("%d:%02d", 
			travelTime.getSeconds() / 3600, (travelTime.getSeconds() % 3600) / 60)+" h");
		
		System.out.println("Koniec podr�y: "+travelEndTime.format(formatter));
	}
}
