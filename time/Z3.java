package com.omiotek.java.time;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjuster;
import java.util.function.Predicate;

public class Z3 {
	public static void main(String[] args) {
		System.out.println("Kolejny dzie� roboczy wypada: "+LocalDate.now().with(nastepny(w-> w.getDayOfWeek().getValue()<6)));
    }

    public static TemporalAdjuster nastepny(Predicate<LocalDate> predicate) {
        return w -> {
            LocalDate wynik = (LocalDate) w;
            do {
            	wynik = wynik.plusDays(1);
            } while (!predicate.test(wynik));
            return wynik;
        };
    }
}
