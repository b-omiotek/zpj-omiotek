package com.omiotek.java.time;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class Z5 {
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Podaj swoj� dat� urodzenia w formacie dd-mm-rrrr:");
		DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd-MM-yyyy");
		String input=s.next().trim();
		s.close();
		if(input.matches("\\d{1,2}-\\d{1,2}-\\d{4}")){
			try{
				System.out.println("�yjesz ju� "+ChronoUnit.DAYS.between(LocalDate.parse(input,formatter),LocalDate.now())+" dni.");
			}
			catch(DateTimeException e){
				System.out.println("Podano z�� dat�.");
				System.exit(1);
			}
		}
		else{
			System.out.println("Podano z�� dat�.");
			System.exit(1);
		}
	}
}
