package com.omiotek.java.time;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Z9 {
	 public static void main(String[] args){
	        ZoneId.getAvailableZoneIds().stream()
	            .map(ZoneId::of)
	            .map(ZonedDateTime::now)
	            .map(t->t.plusHours(1))
	            .filter(w -> w.getOffset().getTotalSeconds()%(3600)!=0)
	            .forEach(System.out::println);
	    }
}
