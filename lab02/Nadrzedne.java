package com.omiotek.java.lab02;

import java.io.Closeable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Nadrzedne {
	public static void main(String[] args) {
		String s = "";
		ArrayList<String> instancje=new ArrayList<>();
        
        if (s instanceof Serializable)
            instancje.add("Serializable");
        if (s instanceof CharSequence)
        	instancje.add("CharSequence");
        if (s instanceof Comparable)
        	instancje.add("Comparable<String>");
        if (s instanceof Object)
        	instancje.add("Object");
        System.out.println("Typy nadrzedne dla String: "+instancje.toString());
        
        Scanner scanner = new Scanner(s);
        instancje.removeAll(instancje);
        if (scanner instanceof Closeable)
        	instancje.add("Closeable");
        if (scanner instanceof AutoCloseable)
        	instancje.add("AutoCloseable");
        if (scanner instanceof Iterator)
        	instancje.add("Iterator");
        if (scanner instanceof Object)
        	instancje.add("Object");
        System.out.println("Typy nadrzedne dla Scanner: "+instancje.toString());
        System.out.println("Typy nadrzedne dla : "+instancje.toString());
    }
}
