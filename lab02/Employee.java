package com.omiotek.java.lab01;

public class Employee implements Measurable{
	private String nazwisko;
	private double wynagrodzenie;

	public static void main(String[] args) {
		Employee[] e=new Employee[2];
		e[0]=new Employee("Kowalski",3500.00);
		e[1]=new Employee("Nowak",2000.00);
		System.out.println("�rednia wynagrodze�: "+getAverage(e)+" z�");
		System.out.println("Najwi�cej zarabia: "+getLargest(e).toString()+" z�");
	}
	
	public Employee(String nazwisko, double wynagrodzenie) {
		this.nazwisko = nazwisko;
		this.wynagrodzenie = wynagrodzenie;
	}
	
	@Override
	public String toString(){
		return nazwisko+" - "+wynagrodzenie;
	}
	
	@Override
	public double getMeasure() {
		return wynagrodzenie;
	}
	
	static double getAverage(Measurable[] objects){
		double srednia =0;
		int i=0;
        for(Measurable e : objects){
        	srednia+=e.getMeasure();
        	i++; 
        }
        return srednia/i;
	}
	
	static Measurable getLargest(Measurable[] objects){
        Measurable m=null;
        double max=0;
        for(Measurable e: objects){
           if(e.getMeasure()>max){
               max = e.getMeasure();
               m = e;
           }
        }
        return m;
	}
}
