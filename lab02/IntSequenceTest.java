package com.omiotek.java.lab02;

public class IntSequenceTest {
	public static void main(String[] args){
		IntSequence is = IntSequence.of(4,5,1,0,6,3);
        while (is.hasNext()) {
            System.out.print(is.next()+" ");
        }
        System.out.println("");
        IntSequence ic = IntSequence.constant(0);
        for (int i=0;i<10;i++){
            System.out.print(ic.next()+" ");
        }
	}
}
