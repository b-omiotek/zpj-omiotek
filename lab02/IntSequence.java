package com.omiotek.java.lab02;

public interface IntSequence {
	default boolean hasNext() {
        return true;
    }
    public static IntSequence of(int... args) {
        return new IntSequence() {
            private int i=0;
            
            @Override
            public int next() {
                return args[i++];
            }
            @Override
            public boolean hasNext() {
                return i<args.length;
            }
        };
    }
    
    public static IntSequence constant(int e){
        return() -> e;
    }
    
    int next();
}
