package com.omiotek.java.lab01;

public interface Measurable {
	public double getMeasure();
}
