package com.omiotek.java.lab01;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import com.opencsv.CSVReader;

public class OpenCSV {
	
	public static void main(String args[]) throws IOException{
		try{
			CSVReader r=new CSVReader(new FileReader("/home/bartek/Pulpit/a.csv"),',','"',1);
			String[] linia;
			while((linia=r.readNext())!= null){
				System.out.println(Arrays.toString(linia));
			}
			r.close();
		}
		catch(ArrayIndexOutOfBoundsException e){
			System.out.println("Podaj plik wejsciowy dla programu!");
		}
		catch(FileNotFoundException e){
			System.out.println("Podany plik wejsciowy nie istnieje!");
		}
	}
}

