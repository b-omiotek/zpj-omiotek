package com.omiotek.java.lab01;
public class Car {
	final double pojemnosc;
	double spalanie;
	double zbiornik;
	
	public static void main(String args[]){
		Car autko=new Car(60.0,6.7,0.0);
		Car.tankujDoPelna(autko);
		Car.sprawdzZbiornik(autko);
		Car.jedz(autko, 142);
		Car.jedz(autko, 14212);
		Car.tankujDoPelna(autko);
		Car.jedz(autko, 222);
	}
	
	public Car(double pojemnosc, double spalanie, double zbiornik) {
		this.pojemnosc = pojemnosc;
		this.spalanie = spalanie;
		this.zbiornik = zbiornik;
	}
	
	public static void tankujDoPelna(Car a){
		a.zbiornik=a.pojemnosc;
		System.out.println("Zatankowano do pelna.");
	}
	
	public static void tankuj(Car a,double paliwo){
		if((a.zbiornik+=paliwo)>a.pojemnosc){
			a.zbiornik=a.pojemnosc;
			System.out.println("Zatankowano do pelna.");
		}
		else{
			a.zbiornik+=paliwo;
			System.out.println("Zatankowano "+paliwo+" litrow");
		}
	}
	
	public static void sprawdzZbiornik(Car a){
		if(a.zbiornik>1)
			System.out.println("W zbiorniku pozostalo: "+Math.round(a.zbiornik)+" litrow paliwa");
		else
			System.out.println("Zbiornik jest pusty!");
	}
	
	public static boolean jedz(Car a,double odleglosc){
		int kilometry=0;
		System.out.println("Probujesz przejechac "+odleglosc+" km");
		while(kilometry!=odleglosc){
			if(a.zbiornik<a.spalanie/100){
				System.out.println("O nie! W zbiorniku zabraklo paliwa.");
				System.out.println("Przejechano "+kilometry+" km");
				sprawdzZbiornik(a);
				return false;
			}
			a.zbiornik-=a.spalanie/100;
			kilometry++;
		}
		if(kilometry==odleglosc){
			System.out.println("Dojechales do celu!");
			sprawdzZbiornik(a);
		}
		return true;
	}
}
