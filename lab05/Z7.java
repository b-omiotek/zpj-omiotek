package com.omiotek.java.lab05;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Z7 {
	public static void main(String[] args) throws IOException {
        Stream<String> zawartosc = Stream.of(new String(Files.readAllBytes(
                Paths.get("C:\\Users\\Lechu\\Desktop\\a.txt")), StandardCharsets.UTF_8).split("\\PL+"));
        
        zawartosc.filter((i) -> Z6.isAlphabetic(i)).limit(100).forEach(i->System.out.print(i+" "));
        System.out.println();
    }
}
