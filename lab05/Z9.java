package com.omiotek.java.lab05;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Z9 {
	public static void main(String[] args) {
        Stream<String> napisy = Stream.of("Ala","ma","kota");
        System.out.println("Srednia dlugosc napisu wynosi: "+length(napisy));
    }

    public static double length(Stream<String> ciag) {
        IntStream dlugosci = ciag.mapToInt(String::length);
        return dlugosci.average().orElse(-1);
    }
}
