package com.omiotek.java.lab05;

import java.util.stream.IntStream;

public class Z5 {
	 public static void main(String[] args) {
	        String s = "string";
	        IntStream litery = (s.chars().limit(s.length()-1));
	        litery.forEach(c -> System.out.println((char)c));
	    }
}
