package com.omiotek.java.lab05;

import java.util.stream.Stream;

public class Z4 {
	public static void main (String [] args){
        long seed=0,a=1,c=32,m=322;
        Stream<Long> generator = Stream.iterate(seed,(x)-> (a * x + c) % m);
        generator.limit(300).forEach(System.out::println);
    }
}
