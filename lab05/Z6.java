package com.omiotek.java.lab05;

import java.util.stream.Stream;

public class Z6 {
	 public static void main(String[] args) {
		Stream<String> alpha = Stream.of("Ala","ma!+","...");
		alpha.forEach( (i) -> {
                System.out.println("isAlphabetic: \"" +i+ "\" : "
                + isAlphabetic(i));
                return;
        });
        Stream<String> java = Stream.of("asc1asd1","aaa","###");
        java.forEach( (i) -> {
                System.out.println("isJavaIdentifier: \"" +i+ "\" : "
                + isJavaIdentifier(i));
                return;
        });
	 }

    public static boolean isAlphabetic(String s) {
        long count = s.codePoints().filter(c ->!Character.isAlphabetic(c)).count();
        if (count==0) 
            return true;
        return false;             
    }    
    public static boolean isJavaIdentifier(String s) {
        long count = s.codePoints().filter(c ->!Character.isJavaIdentifierPart(c)).count();
        if (count==0) 
            return true;
        return false;           
    }
}
