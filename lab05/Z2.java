package com.omiotek.java.lab05;

import java.util.ArrayList;
import java.util.Arrays;

public class Z2 {
   public static void main(String[] args){
        long currentTimeMillis = System.currentTimeMillis();
        stream();
        double wynik= System.currentTimeMillis()-currentTimeMillis;
        currentTimeMillis = System.currentTimeMillis();
        pararelStream();
        double wynik2 = System.currentTimeMillis()-currentTimeMillis;
        if(wynik<wynik2)
        	System.out.println("Stream jest szybszy.");
        else
        	System.out.println("PararelStream jest szybszy." );
    }
	public static void stream(){
        long ile;
        ArrayList<String> words = new ArrayList<>(Arrays.asList("Ala","ma","kota"));
        ile = words.stream().filter(w -> w.length() > 12).count();
    }

    public static void pararelStream(){
        long ile;
        ArrayList<String> words = new ArrayList<>(Arrays.asList("Ala","ma","kota"));
        ile = words.parallelStream().filter(w -> w.length() > 12).count();

    }
}
