package com.omiotek.java.lab05;

import java.util.ArrayList;
import java.util.Arrays;

public class Z1 {
	public static void main(String[] args){
        ArrayList<String> lista = new ArrayList<>(Arrays.asList("Alaaaaaaaaaaaa","maaaaaaaaaaaaaaaaaa","kotaaaaaaaaaaaaaaaaaaaaaaaa","małeaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaago","w","czarneeeeeeeeeeeeeeeeeeeeeeeee","kroooooooooooooooooooopki"));

        long ile = lista.stream()
                .filter(w -> w.length() > 10)
                .peek((i) -> System.out.println(i))
                .limit(5).count();

        ile = lista.parallelStream().filter(w -> w.length() > 12).count();
        System.out.println("Znaleziono: "+ile);
    }
}
