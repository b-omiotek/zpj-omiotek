package com.omiotek.java.lab05;

import java.util.stream.Stream;

public class Z11 {
	public static void main(String[] args){
	    Stream<Long> stream = Stream.iterate(1L, s->s+1);
	    System.out.println(isFinite(stream));
	}
	
    public static <T> boolean isFinite(Stream<T> stream){
    	if(50000000000L<stream.spliterator().estimateSize())
            return true;
        return false;
    }
}
