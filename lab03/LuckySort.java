package com.omiotek.java.lab01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class LuckySort {
	
	public static void main(String[] args) {
		ArrayList<String> lista=new ArrayList<>();
		lista.add("aba");
		lista.add("bab");
		lista.add("aaa");
		
		Comparator<String> comp=new Comparator<String>(){
			public int compare(String a,String b){
				if(a.compareTo(b)==0)
					return 0;
				return a.compareTo(b);
			}
		};
		
		LuckySort(lista,comp);
		System.out.println(lista.toString());
	}
	public static void LuckySort(ArrayList<String> lista,Comparator<String> comp){
		for(int i=0;i<lista.size()-1;i++){
				if ((comp.compare(lista.get(i), lista.get(i+1))) != -1){
					Collections.shuffle(lista);
					i=-1;
                }	
		}
	}
}
