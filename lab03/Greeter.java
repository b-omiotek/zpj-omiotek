package com.omiotek.java.lab03;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Greeter implements Runnable{
	private int n;
    private String target;
    
    public static void main(String[] args) {
    	Greeter gr1 = new Greeter(200, "Bartek");
        Greeter gr2 = new Greeter(300, "Grzesiek");
        Thread th1 = new Thread(gr1);
        Thread th2 = new Thread(gr2);
        
        //gr1.run();
        //Greeter.runTogether(gr1,gr2);
        Greeter.runInOrder(gr1,gr2);
	}
    
    public Greeter(int n, String target){
        this.n = n;
        this.target = target;
    }

    @Override
    public void run() {
        for(int i=0;i<n;i++){
            System.out.println("Witaj  "+ target);
        }
    }
    
    public static void runTogether(Runnable...task){
        Thread[] watki = new Thread[task.length];
        for(int i = 0;i<task.length;i++){
            watki[i] =new Thread(task[i]);
            watki[i].start();            
        }
        System.exit(0);
    }
    
    public static void runInOrder(Runnable...task){
        for(int i = 0;i<task.length;i++){
            Thread watek = new Thread(task[i]);
            watek.start();
            try {
                watek.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Greeter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
        
}
